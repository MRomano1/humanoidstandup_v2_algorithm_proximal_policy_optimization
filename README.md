
This is an implementation of the ppo algorithm applied to the mujoco gym environment provided by Open-AI. Our objective is to be able to get the robot stand up using the ppo algorithm defining a proper reward function based on state space and avaible actions. You can refer to [this link](https://gymnasium.farama.org/) to see the main environments provided and the **image** below as our implementation overview result:


![image](humanoid_sit.png)
